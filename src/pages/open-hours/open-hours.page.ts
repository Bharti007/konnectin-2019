import { Component, OnInit } from '@angular/core';
import { CallService } from '../../services/call.service';
import { PageInfoService } from '../home/page-info.service';

@Component({
	templateUrl: 'open-hours.page.html'
})
export class OpenHoursPage implements OnInit {
	info: any;

	constructor(private service: PageInfoService, private callService: CallService) {
	}

	ngOnInit(): any {
		this.service.getInfo()
			.subscribe(info => {
				this.info = info;
			});
	}

	call() {
		this.callService.call(this.info.phone);
	}
}
