import {Component, NgZone, ViewChild} from '@angular/core';
import { NavController, NavParams,Content,ViewController } from 'ionic-angular';
declare var window;

/**
 * Generated class for the ChatbotPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-chatbot',
  templateUrl: 'chatbot.html',
})
export class ChatbotPage {
  messages:any=[];
    chatText:any='';
    @ViewChild(Content) content:Content

  constructor(public navCtrl: NavController, public navParams: NavParams,public ngZone : NgZone,public viewCtrl: ViewController) {

    console.log(this.navParams);

    if(this.navParams.data && this.navParams.data.chatText){
        this.messages.push({
            text:this.navParams.data.chatText,
            sender:"me"
        });
        this.apisAitextcall(this.navParams.data.chatText);

    }
    else{
        this.messages.push({
            text:"Hola! how can i help you?",
            sender:"api"
        });
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatbotPage');
  }


  sendText(){
    let message = this.chatText;
      this.chatText = '';
      this.messages.push({
          text:message,
          sender:"me"
      });
      this.content.scrollToBottom(600);

     this.apisAitextcall(message);

  }

    apisAitextcall(message){
    window['ApiAIPlugin'].requestText(
        {
            query:message
        },
(response)=> { /* success processing */
    console.log("apiai successful");
    this.ngZone.run(()=>{
    this.messages.push({
                           text:response.result.fulfillment.speech,
    sender:"api"
})
        this.content.scrollToBottom(600);
})

},
(error)=> { /* error processing */ }
);
}

    dismiss() {
        this.viewCtrl.dismiss();
    }

}
