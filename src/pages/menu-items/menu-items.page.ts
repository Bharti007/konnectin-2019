import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { MenuItemPage } from './manu-item.page';

import {FacebookApiService} from "../../services/facebook-api.service";
import {NewsListPage} from "../news/list/news.list.page";
import {BusinessesPage} from "../businesses/businesses";

@Component({
	templateUrl: './menu-items.page.html',
	selector: 'as-page-menu-items'
})
export class MenuItemsPage {
	items: any=[];
    tiles :any =[];
	constructor(private menudata: FacebookApiService, private nav: NavController) {
		this.items = menudata.getMenuItems();
		this.initTiles();

    }

	public itemTapped(item) {
		this.nav.push(MenuItemPage, {
			item: item
		});
	}

    private initTiles(): void {
        this.tiles = [[
            {
                title: 'Online Bookings',
                path: 'service',
                icon: 'calender',
                image: 'assets/img/online-booking.jpeg',
                component: MenuItemsPage ,
            },{
                title: 'Report Issues',
                path: 'news',
                icon: 'list-box',
                image: 'assets/img/report-issues.jpeg',
                component: NewsListPage
            }
            ,{
                title: 'Waste Collection',
                path: 'group',
                icon: 'list-box',
                image: 'assets/img/waste-collection.jpeg',
                component: NewsListPage
            },{
                title: 'Pay Bills',
                path: 'localbusiness',
                icon: 'paper',
                image: 'assets/img/paybills.jpeg',
                component:BusinessesPage
            },{
                title: 'Library',
                path: 'group',
                icon: 'list-box',
                image: 'assets/img/library.jpeg',
                component: NewsListPage
            },{
                title: 'Contact Us',
                path: 'localbusiness',
                icon: 'paper',
                image: 'assets/img/contact-us.jpeg',
                component:BusinessesPage
            }



        ]];
    }
}
