import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { CallService } from '../../services/call.service';
import { EmailService } from '../../services/email.service';
import { InAppBrowserService } from '../../services/in-app-browser.service';
import { MapsService } from '../../services/maps.service';
import { OpenHoursService } from '../../services/open-hours.service';

@Component({
	selector: 'page-contact-us',
	templateUrl: './contact-us.html'
})
export class ContactUsPage {
	business: any;
	days: any[];

	constructor(
		navParams: NavParams,
		private callService: CallService,
		private emailService: EmailService,
		private inBrowser: InAppBrowserService,
		private openHoursService: OpenHoursService,
		private mapsService: MapsService
	) {
		this.business = navParams.get('business');
		this.days = this.openHoursService.getOpenHours(this.business.openhours);
	}

	call(phone: string) {
		this.callService.call(phone);
	}

	sendEmail(email) {
		this.emailService.sendEmail(email);
	}

	openUrl(url: string) {
		this.inBrowser.open(url);
	}

	getDirections(officeLocation: string) {
		this.mapsService.openMapsApp(officeLocation, this.business.name);
	}
}
