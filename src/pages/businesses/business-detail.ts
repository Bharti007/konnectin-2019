import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MapsService } from '../../services/maps.service';
import { OpenHoursService } from '../../services/open-hours.service';
import { BusinessesService } from '../../services/businesses.service';
import { ContactUsPage } from './contact-us';

@Component({
	selector: 'page-business-detail',
	templateUrl: './business-detail.html'
})
export class BusinessDetailPage {
	business: any;

	isOpen: boolean;

	constructor(
		public service: BusinessesService,
		private navCtrl: NavController,
		openHoursService: OpenHoursService,
		private mapsService: MapsService
	) {
		this.business = service.getCurrent();
		this.isOpen = this.business.openhours && openHoursService.isBusinessOpen(this.business.openhours);
	}

	getDirections(officeLocation: string) {
		this.mapsService.openMapsApp(officeLocation, this.business.name);
	}

	goToContactUs(business: any) {
		this.navCtrl.push(ContactUsPage, { business: business });
	}

	navigateTo(tile: any) {
		this.navCtrl.push(tile.component);
	}
}
