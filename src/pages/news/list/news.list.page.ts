import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsService } from '../news.service';
import { NewsItemPage } from '../item/news.item.page';

import { AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import {CalendarDateFormatter} from "angular-calendar";
import {CustomDateFormatter} from "../../events/custom-date-formatter";

import { isSameDay, isSameMonth } from 'date-fns';

import {FacebookApiService} from "../../../services/facebook-api.service";

@Component({
	selector:'news-list',
	templateUrl: 'news.list.html',
    providers: [{
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter
    },
        NewsService
    ]
})
export class NewsListPage implements OnInit {
	private service: NewsService;
	private nav: NavController;
	public posts: any[];
	public postcollection:any;

    eventt: any=[];
    viewDate: Date = new Date();
    view = 'month';
    activeDayIsOpen: boolean = false;

    events: any[] = [];
    action: string = "news";
	constructor(service: NewsService, nav: NavController , public afstore: AngularFirestore,dataService: FacebookApiService) {
		this.service = service;
		this.nav = nav;
        this.postcollection = this.afstore.collection('posts');


        setTimeout(function () {
            this.eventt = dataService.getEvents();
            console.log(this.eventt)
        },5000)

    }

	ngOnInit(): void {

        this.postcollection.valueChanges().subscribe(posts=>{
        	console.log(posts)
            this.posts = posts;
		}
	)

	}

	public itemTapped(item) {
		this.nav.push(NewsItemPage, {
			item: item
		});
	}

    public doRefresh(refresher) {
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }

    dayClicked({ date, events }: { date: Date, events: any[] }): void {

        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
    }

    viewDateChange() {
        this.activeDayIsOpen = false;
    }
}
