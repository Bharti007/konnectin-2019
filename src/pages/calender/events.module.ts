import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { EventsPage } from './events.page';
import { CalendarModule } from 'angular-calendar';

@NgModule({
	imports: [IonicModule, PipesModule, CalendarModule],
	declarations: [
		EventsPage
	],
	entryComponents: [
		EventsPage
	]
})
export class EventsCalModule {

}