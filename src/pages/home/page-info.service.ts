import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { FacebookApiService } from '../../services/facebook-api.service';

@Injectable()
export class PageInfoService {
	private facebookApiService: FacebookApiService;
	private days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
	private info: any;

	constructor(facebookApiService: FacebookApiService, private http: Http) {
		this.facebookApiService = facebookApiService;
	}

	public getInfo(): Observable<any> {
		if (this.info) {
			return Observable.of(this.info);
		}

		return this.facebookApiService
			.getInfo()
			.flatMap((x: any) => {
				return this.getTimezone(x.location.latitude, x.location.longitude)
					.map(utcOffset => {
						let hours = this.mapHours(x.hours);
						this.info = Object.assign(x, {
							callToActions: PageInfoService.getCallToActions(x.call_to_actions),
							address: PageInfoService.getAddress(x),
							hours: hours,
							isOpen: this.checkIfOpen(hours, utcOffset)
						});
						return this.info;
					});
			});
	}

	private getTimezone(lat, lon) {
		let timestamp = new Date().getTime().toString();
		timestamp = timestamp.substr(0, 8);
		let url = `https://maps.googleapis.com/maps/api/timezone/json?location=${lat},${lon}&timestamp=${timestamp}`;
		return this.http.get(url)
			.map(x => x.json())
			.map(x => (x.rawOffset + x.dstOffset));
	}

	private static getAddress(x) {
		let parts = [];

		if (x.location) {
			parts.push(x.location.street);
			parts.push(x.location.city);
			parts.push(x.location.state);
			parts.push(x.location.zip);
			parts.push(x.location.country);
		}

		return parts.filter(x => !!x).join(', ');
	}

	private static getCallToActions(callToActions: any) {
		return callToActions && callToActions.data[0];
	}

	private mapHours(hours: any = {}) {
		let openHours = {};

		Object.keys(hours).forEach(key => {
			let arr = key.split('_');
			let slotNumber = arr[1];
			let marker = arr[2];
			let day = PageInfoService.getDay(arr[0]);
			if (!openHours[day]) {
				openHours[day] = {};
			}
			if (!openHours[day][slotNumber]) {
				openHours[day][slotNumber] = {};
			}

			openHours[day][slotNumber][marker] = hours[key];
		});

		let openHoursArray = [];

		this.days.forEach(day => {
			if (openHours[day]) {
				openHoursArray.push({
					day: day,
					slots: Object.keys(openHours[day]).map(key => openHours[day][key])
				});
			}
		});

		return openHoursArray;
	}

	private static getDay(day: string) {
		switch (day) {
			case 'mon':
				return 'Monday';
			case 'tue':
				return 'Tuesday';
			case 'wed':
				return 'Wednesday';
			case 'thu':
				return 'Thursday';
			case 'fri':
				return 'Friday';
			case 'sat':
				return 'Saturday';
			case 'sun':
				return 'Sunday';
			default:
				throw new Error('Wrong day');
		}
	}

	private checkIfOpen(hours: any, locationOffsetInSeconds: number) {
		let now = new Date();
		let currentOffsetInMs = now.getTimezoneOffset();
		let utcMs = now.getTime() + (currentOffsetInMs * 60 * 1000);
		let locationMs = utcMs + (locationOffsetInSeconds * 1000);

		let locationDate = new Date(locationMs);
		let locationDay = this.days[locationDate.getDay() - 1];

		let dayHours = hours.find(slot => slot.day === locationDay);
		if (!dayHours) {
			return false
		}

		return dayHours.slots.some(slot => {
			let openSlot = slot.open.split(':');
			let closeSlot = slot.close.split(':');
			let openHours = parseInt(openSlot[0], 10);
			let openMinutes = parseInt(openSlot[1], 10);
			let closeHours = parseInt(closeSlot[0], 10);
			let closeMinutes = parseInt(closeSlot[1], 10);

			let minutesFromMidnight = (locationDate.getHours() * 60) + locationDate.getMinutes();
			if (minutesFromMidnight < (openHours * 60) + openMinutes) {
				return false;
			}

			if (minutesFromMidnight > (closeHours * 60) + closeMinutes) {
				return false;
			}

			return true;
		});
	}
}