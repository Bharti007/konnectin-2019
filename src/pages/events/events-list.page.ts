import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EventsService } from './events.service';
import { EventDetailsPage } from './event-details.page';

@Component({
	selector: 'events-list',
	templateUrl: 'events-list.page.html'
})
export class EventsListPage implements OnInit {
	private service: EventsService;
	private nav: NavController;
	public events: any[];

	constructor(service: EventsService, nav: NavController) {
		this.service = service;
		this.nav = nav;
	}

	ngOnInit(): void {
		this.service.getEvents()
			.subscribe(events => {
				this.events = events;
				console.log(events);
			});
	}

	public itemTapped(item) {
		this.nav.push(EventDetailsPage, {
			item: item
		});
	}
}
