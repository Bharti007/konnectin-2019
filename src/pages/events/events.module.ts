import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { EventDetailsPage } from './event-details.page';
import { EventsListPage } from './events-list.page';
import { EventsService } from './events.service';
import {CalendarModule} from "angular-calendar";

import { EventsPage } from './events.page';//calender page

@NgModule({
	imports: [IonicModule, PipesModule, CalendarModule],
	declarations: [
		EventDetailsPage,
		EventsListPage,
        EventsPage
	],
	entryComponents: [
		EventDetailsPage,
		EventsListPage,
        EventsPage
	],
	providers: [EventsService]
})
export class EventsModule {

}