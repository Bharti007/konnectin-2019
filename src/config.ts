import { Injectable } from '@angular/core';
import { CloudSettings } from '@ionic/cloud';

@Injectable()
export class Config {
	// http://logicum.co/getting-a-facebook-page-permanent-access-token/
	static facebookPermanentAccessToken = 'EAAP3RGVwMMIBABlgPPH8ynzMcbqabY6aEDmdNTgeXtRUo18Jf5wmRcpVIfk4ZBoD6WKGzZA32j25UYtj1Q2O11MXKXTBMldFnvs02axn7iPuf7tyZA3NpBTTOXj22sLtsDegPyTO8edvWoZBoCfZBrFu1xuaWXxmHkEoobbtQ8QZDZD';
	static apiUrl = 'https://graph.facebook.com/v2.8/';
	static pageName = 'edesianyc';
	static pushProvider: 'IONIC' | 'ONE_SIGNAL' = 'ONE_SIGNAL';
}

export const sender_id = '228071472080';
const app_id = '241b6d37';

export const cloudSettings: CloudSettings = {
	core: {
		app_id: app_id
	},
	push: {
		sender_id: sender_id
	}
};

export const oneSignalSettings = {
	appId: '8046df2e-979e-4333-aeae-95a81bbc950e',
};
