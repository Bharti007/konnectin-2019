import { AgmCoreModule } from '@agm/core';
import { ErrorHandler, NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Calendar } from '@ionic-native/calendar';
import { EmailComposer } from '@ionic-native/email-composer';
import { StatusBar } from '@ionic-native/status-bar';
import { CloudModule } from '@ionic/cloud-angular';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CustomComponentsModule } from '../components/custom-components.module';
import { FlashCardModule } from '../components/flash-card/flash-card.module';

import { cloudSettings, Config } from '../config';
import { AlbumsModule } from '../pages/albums/albums.module';
import { EventsModule } from '../pages/events/events.module';
import { HomeModule } from '../pages/home/home.module';
import { MapModule } from '../pages/map/map.module';
import { NewsModule } from '../pages/news/news.module';
import { OpenHoursModule } from '../pages/open-hours/open-hours.module';
import { ReviewsModule } from '../pages/reviews/reviews.module';
import { Base64Service } from '../services/base64.service';
import { FacebookApiService } from '../services/facebook-api.service';
import { MyApp } from './app.component';

import { MenuItemsModule } from '../pages/menu-items/menu-items.module';


import { PushModule } from '../pages/push/push.module';

import { IonicCloudPushListener } from '../services/ionic-cloud.push-listener';
import {ChatbotPage} from "../pages/chatbot/chatbot";
import {CommunityGroupPage} from "../pages/community-group/community-group";
//import { OneSignalPushListener } from '../services/one-signal.push-listener';
//import { OneSignal } from '@ionic-native/onesignal';



import { LoginPage } from '../pages/login/login';
 import { ProfilePage } from '../pages/profile/profile';
import { TabsPage } from '../pages/tabs/tabs';
import { HomeTabsPage } from '../pages/home-tabs/home-tabs';
import { MessagesPage } from '../pages/messages/messages';
import { GroupsPage } from '../pages/groups/groups';
import { FriendsPage } from '../pages/friends/friends';
import { MessagePage } from '../pages/message/message';
import { GroupPage } from '../pages/group/group';
import { GroupInfoPage } from '../pages/group-info/group-info';
import { NewGroupPage } from '../pages/new-group/new-group';
import { AddMembersPage } from '../pages/add-members/add-members';

import { LoginProvider } from '../providers/login';
import { LogoutProvider } from '../providers/logout';
import { LoadingProvider } from '../providers/loading';
import { AlertProvider } from '../providers/alert';
import { ImageProvider } from '../providers/image';
import { DataProvider } from '../providers/data';
import { FirebaseProvider } from '../providers/firebase';

import * as firebase from 'firebase';


import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {AngularFirestore} from 'angularfire2/firestore';

import { Settings } from '../settings';

import { FriendPipe } from '../pipes/friend';
import { SearchPipe } from '../pipes/search';
import { ConversationPipe } from '../pipes/conversation';
import { DateFormatPipe } from '../pipes/date';
import { GroupPipe } from '../pipes/group';

import { SplashScreen } from '@ionic-native/splash-screen';
import { GooglePlus } from '@ionic-native/google-plus';
import { Camera } from '@ionic-native/camera';
import { Keyboard } from '@ionic-native/keyboard';
import { Contacts } from '@ionic-native/contacts';
import { MediaCapture } from '@ionic-native/media-capture';
import { File } from '@ionic-native/file';
import { Geolocation } from '@ionic-native/geolocation';
import { Firebase } from '@ionic-native/firebase';
// import { Facebook } from '@ionic-native/facebook';
import { HttpModule } from '@angular/http';

import { HttpClientModule ,HttpClient} from '@angular/common/http';
import {EventsCalModule} from "../pages/calender/events.module";

import { CalendarModule } from 'angular-calendar';
import {BusinessesService} from "../services/businesses.service";
import {BusinessesModule} from "../pages/businesses/businesses.module";
import {MapsService} from "../services/maps.service";
import {OpenHoursService} from "../services/open-hours.service";
import {DistancePipe} from "../pipes/distance.pipe";
import {FavoritesModule} from "../pages/favorites/favorites.module";
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
firebase.initializeApp(Settings.firebaseConfig);



@NgModule({
	declarations: [
		MyApp,
		 LoginPage, 
    TabsPage,
    MessagesPage,
    GroupsPage,
    GroupInfoPage,
    FriendsPage,
    MessagePage,
    GroupPage,
    NewGroupPage,
    AddMembersPage,
    FriendPipe,
    ConversationPipe,
    SearchPipe,
    DateFormatPipe,
    GroupPipe,
    ProfilePage,
    HomeTabsPage
        ,DistancePipe,
        ChatbotPage,
        CommunityGroupPage
	],
	imports: [
		IonicModule.forRoot(MyApp, {
      scrollAssist: false,
      autoFocusAssist: false,
      mode: 'ios',
      tabsPlacement: 'bottom'
	}),
	
    AngularFireModule.initializeApp(Settings.firebaseConfig,'firechat'),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
		AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAICFcc8EqgpMaytQEuPkcYAFaCFQXNfRo'
        }),
        AgmSnazzyInfoWindowModule,
		CloudModule.forRoot(cloudSettings),
		CustomComponentsModule,
		BrowserModule,
		NewsModule,
		HomeModule,
		AlbumsModule,
		EventsModule,
		OpenHoursModule,
		ReviewsModule,
    PushModule,
    FlashCardModule,
        HttpModule,
        HttpClientModule,
        MenuItemsModule,
        BusinessesModule,
        FavoritesModule,
        MapModule,
        EventsCalModule,CalendarModule.forRoot()

	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		LoginPage,
    TabsPage,
    MessagesPage,
    GroupsPage,
    FriendsPage,
    MessagePage,
    GroupPage,
    GroupInfoPage,
    NewGroupPage,
    AddMembersPage,
    ProfilePage,
    HomeTabsPage,
        ChatbotPage,
        CommunityGroupPage
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
	providers: [
		StatusBar,
		Calendar,
		EmailComposer,
        IonicCloudPushListener,
		Config,
		Base64Service,
		FacebookApiService,
        BusinessesService,
		SplashScreen,
    GooglePlus,
    Camera,
    Keyboard,
    Contacts,
    MediaCapture,
    File,
    Geolocation,
    Firebase,
    LoginProvider,
    LogoutProvider,
    LoadingProvider,
    AlertProvider,
    ImageProvider,
    DataProvider,
    FirebaseProvider,
        AngularFirestore,MapsService,
        OpenHoursService,

		{ provide: ErrorHandler, useClass: IonicErrorHandler }
	]
})
export class AppModule {
}
