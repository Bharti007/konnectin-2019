import { Component, ViewChild ,NgZone} from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { App, MenuController, Nav, Platform,AlertController } from 'ionic-angular';
import { AlbumsListPage } from '../pages/albums/albums-list.page';
import { EventsListPage } from '../pages/events/events-list.page';
import { HomePage } from '../pages/home/home.page';
import { ProfilePage } from '../pages/profile/profile';
import { ContactUsPage } from '../pages/home/contact-us.page';
// import { MapPage } from '../pages/map/map.page';
import { LoginPage } from '../pages/login/login';
import { NewsListPage } from '../pages/news/list/news.list.page';
// import { OpenHoursPage } from '../pages/open-hours/open-hours.page';
// import { ReviewsPage } from '../pages/reviews/reviews.page';
 import { IonicCloudPushListener } from '../services/ionic-cloud.push-listener';
// import { OneSignalPushListener } from '../services/one-signal.push-listener';
import { Config } from '../config';

import { SplashScreen } from '@ionic-native/splash-screen';
import  {Firebase} from '@ionic-native/firebase';
import * as firebase from 'firebase';
import { LogoutProvider } from '../providers/logout';
// import { AlertProvider } from '../providers/alert';
import { HomeTabsPage } from '../pages/home-tabs/home-tabs';
import { MessagesPage } from '../pages/messages/messages';

import { AngularFirestore } from  'angularfire2/firestore';
declare  var window;
@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    wide: boolean = false;
    genericPages;
    homePage;
    rootPage;
    private alert;
    private app;
    private platform;
    private menu: MenuController;

    @ViewChild(Nav) nav: Nav;

    constructor(
        platform: Platform,
        menu: MenuController,
        app: App,
        private statusBar: StatusBar,
       public splashScreen: SplashScreen,
        public alertCtrl: AlertController,public logoutProvider: LogoutProvider,
        public cordovaFirebase:Firebase,public zone:NgZone,
        public afs: AngularFirestore
    ) {
        this.menu = menu;
        // set up our app
        this.app = app;
        this.platform = platform;
        this.initializeApp();
        afs.firestore.settings({timestampsInSnapshots: false});

        // set our app's pages
        // this.homePage = { title: 'Home', component: HomePage, icon: 'home' };

        this.genericPages = [
            { title: 'Home', component: HomeTabsPage, icon: 'home' },
            // { title: 'News & Events', component: NewsListPage, icon: 'paper' },
            // { title: 'Chat', component: MessagesPage, icon: 'chatbubbles' },
            // { title: 'Map', component: MapPage, icon: 'map' },
            { title: 'Profile', component: ProfilePage, icon: 'contacts' },
            { title: 'Contact Us', component: ContactUsPage, icon: 'call' },
            { title: 'Feedback', component: ProfilePage, icon: 'clipboard' },
            { title: 'Reviews', component: ProfilePage, icon: 'star-half' }
        ];

        this.rootPage = LoginPage;
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();


            firebase.auth().onAuthStateChanged((user) => {
                if (user) {

                    this.cordovaFirebase.grantPermission();

                    this.cordovaFirebase.getToken()
                        .then(token => {
                            firebase.database().ref('accounts/' + user.uid + '/pushToken').set(token);
                        })
                        .catch(error => console.log(error));

                    this.zone.run(()=>{
                        this.rootPage = HomeTabsPage;
                    });
                } else {
                    this.zone.run(()=>{
                        this.rootPage = LoginPage;
                    });

                }
            });

            this.platform.pause.subscribe(()=>{
                if(firebase.auth().currentUser)
                    firebase.database().ref('accounts/'+firebase.auth().currentUser.uid).update({'online': false});
            });
            this.platform.resume.subscribe(()=>{
                if(firebase.auth().currentUser && localStorage.getItem('showOnline') == 'true')
                    firebase.database().ref('accounts/'+firebase.auth().currentUser.uid).update({'online': true});
            });


            window['ApiAIPlugin'].init(
                {
                    clientAccessToken: "e4d2039112f8499381a965a26b9e3912", // insert your client access key here
                    lang: "en" // set lang tag from list of supported languages
                },
                function(result) { /* success processing */
                    console.log("apiai successful");
                    },
                function(error) { /* error processing */ }
            );
        });
    }

    openPage(page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        let component = page.component;

        this.nav.push(component);
    }

    setWidth() {
        if (this.platform.width() > 767) {
            this.wide = true;
            this.menu.open();
        } else {
            this.wide = false;
            this.menu.close();
        }
    };

    listenToEvents() {
        window.addEventListener('resize', () => {
            this.setWidth();
        });
    }

    // Log the user out.
    logout() {
        this.alert = this.alertCtrl.create({
            title: 'Confirm Logout',
            message: 'Are you sure you want to logout?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Logout',
                    handler: data => { this.logoutProvider.logout(); }
                }
            ]
        }).present();
    }
}
