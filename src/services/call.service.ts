import { Injectable } from '@angular/core';

@Injectable()
export class CallService {
	public call(phoneNumber: string): void {
		phoneNumber = phoneNumber.replace(/[^\d+]/g,'');
		window.location.href = 'tel:' + phoneNumber;
	}
}