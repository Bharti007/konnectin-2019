import { Injectable } from '@angular/core';
import { Push, PushToken } from '@ionic/cloud-angular';

@Injectable()
export class IonicCloudPushListener {
	constructor(private push: Push) {
	}

	init() {
		this.push.register().then((t: PushToken) => this.push.saveToken(t));

		this.push.rx.notification().subscribe(this.onPush);
	}

	private onPush(msg) {
		alert('IONIC_CLOUD. ' + msg.title + ': ' + msg.text);
	}
}