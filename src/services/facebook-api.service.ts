import { Injectable } from '@angular/core';
import { Config } from '../config';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';

@Injectable()
export class FacebookApiService {
	private http: Http;
	private pageAccessToken: string;
	public menuList :any=[];
	public events:any=[];

	constructor(http: Http) {
		this.http = http;
	}

	getInfo(): Observable<any> {
		let fields = 'category,hours,cover,emails,location,phone,username,id,name,about,description,website,access_token,';
		fields += 'call_to_actions{email_address,intl_number_with_plus,web_destination_type,destination_type,web_url}';
		return this.http.get(Config.apiUrl + Config.pageName, this.options(fields))
			.map(x => x.json())
			.do(info => {


				if (info != undefined && info != '') {
					info.location = {
						"city": "Sydney",
						"country": "Australia",
						"latitude": -33.884609,
						"longitude": 151.196269,
						"state": "AU",
						"street": "166 Broadway, Chippendale",
						"zip": "NSW 2008"
					}
					info.cover.source=[];
					info.cover.source[0] = "assets/img/banner.jpeg";
                    info.cover.source[1] = "assets/img/banner2.jpeg";
					info.phone = "(02) 9211 5993";
					info.email = "info@broadwaycrown.com.au";
					info.name = "Broadway Crown"
					info.website = "http://www.broadwaycrown.com.au/";
					info.description = "Step into the action at Broadway Crown, formally known as The Broadway Hotel, our lavish art deco venue offers you the perfect setting for after work drinks, cocktails, pizzas, and burgers. Step into the action at Broadway Crown, formally known as The Broadway Hotel, our lavish art deco venue offers you the perfect setting for after work drinks, cocktails, pizzas, and burgers. Allow the ambiance to captivate you, pull up a pew and immerse yourself in hand picked craft beers, signature cocktails and a food menu to suit all tastes offering you pizzas, burgers and classic pub dishes.Located within the hustle of Broadway Chippendale, Sydney - Broadway Crown is picture perfect."
				}
                console.log(info);

				this.pageAccessToken = info.access_token;
			});
	}

	// getEvents() {
	// 	let fields = 'description,name,place,start_time,end_time,cover,category';
	// 	return this.http.get(Config.apiUrl + Config.pageName + '/events', this.options(fields))
	// 		.map(x => x.json())
	// 		.map(result => {
	// 			return result.data;
	// 		});
	// }

	getAlbums() {
		let fields = 'cover_photo,name,description';
		return this.http.get(Config.apiUrl + Config.pageName + '/albums', this.options(fields))
			.map(x => x.json())
			.map(result => {
				var albums = [];
				result.data.forEach(album => {
					let coverPhoto = album['cover_photo'];
					if (coverPhoto) {
						let cover = `${Config.apiUrl}${coverPhoto.id}/picture?access_token=${Config.facebookPermanentAccessToken}`;
						albums.push({
							id: album.id,
							cover: cover,
							title: album.name,
							description: album.description
						});
					}
				});

				return albums;
			});
	}

	getAlbum(albumId: string): Observable<any> {
		let fields = 'name,place,picture,images,created_time';
		let url = Config.apiUrl + albumId + '/photos';
		return this.http.get(url, this.options(fields))
			.map(x => x.json())
			.map(x => x.data);
	}

	getPosts(): Observable<any> {
		let fields = 'posts{created_time,description,picture,full_picture,caption,message,story,type,object_id,link,from,name,place}';
		return this.http.get(Config.apiUrl + Config.pageName, this.options(fields))
			.map(x => x.json())
			.map((x: any) => {
				return x.posts.data;
			});
	}

	getReviews() {
		let fields = 'review_text,created_time,rating,reviewer,has_rating,has_review';
		return this.http.get(`${Config.apiUrl + Config.pageName}/ratings`, this.options(fields, this.pageAccessToken))
			.map(x => x.json())
			.map((x: any) => {
				return x.data;
			});
	}

	private options(fields: string = null, accessToken?: string) {
		let params = new URLSearchParams();
		params.set('access_token', accessToken || Config.facebookPermanentAccessToken);
		if (fields) {
			params.set('fields', fields);
		}
		return new RequestOptions({
			search: params,

		});
	}

    getMenuItems(){
		this.menuList = [
            {
                "body": "The technician did the servicing really well with a good result. All our 5 Acs are working fine now. Appreciate knowledge of fixing all brands of AC both split and window",
                "category": "room service",
                "isFeatured": false,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "assets/img/acservice1.jpg",
                        "url": "assets/img/acservice1.jpg"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Standard",
                        "value": "13.59"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Refrigerator Repair",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "assets/img/acservice.jpeg",
                        "url": "assets/img/acservice.jpeg"
                    }
                ],
                "title": "AC service (50% OFF)"
            },
            {
                "body": "Moments of rejuvenation in a cool ambience is what New Beauty Spa promises to its customers. Offering a plethora of massage therapies, it is definitely one-of-its-kinds in India. Situated in the heart of the city yet encapsulated in a calm and peaceful abode, this massage parlour is a blessing to the weary city-dwellers.",
                "category": "beauty",
                "extraOptions": [
                    {

                        "name": "Mehendi Artists",
                        "selected": false,
                        "value": 3.15
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "assets/img/beauty2.jpeg",
                        "url": "assets/img/beauty2.jpeg"
                    },
                    {
                        "inProgress": false,
                        "path": "assets/img/beauty1.jpg",
                        "url": "assets/img/beauty1.jpg"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Single",
                        "value": 8
                    },
                    {
                        "currency": "$",
                        "name": "Double",
                        "value": 13
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Salon at Home",
                        "selected": true
                    },
                    {
                        "name": "Makeup & Hair-Styling",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "assets/img/beauty.jpg",
                        "url": "assets/img/beauty.jpg"
                    }
                ],
                "title": "Beauty & Spa"
            },
            {
                "body": "Dirtsmash comes with a complete range of paint, polish, repair, and cleaning services for home, office and industrial space. Our services are available in Sydney",
                "category": "cleaning",
                "extraOptions": [
                    {
                        "name": "Carpet Cleaning",
                        "selected": false,
                        "value": "2.10"
                    },
                    {
                        "name": "Pest Control",
                        "selected": false,
                        "value": "3.20"
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "assets/img/cleaning1.jpg",
                        "url": "assets/img/cleaning1.jpg"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Single",
                        "value": "6"
                    },
                    {
                        "currency": "$",
                        "name": "Double",
                        "value": "11"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Sofa Cleaning",
                        "selected": true
                    },
                    {
                        "name": "Bathroom Deep Cleaning",
                        "selected": true
                    },
                    {
                        "name": "Home Deep Cleaning",
                        "selected": true
                    },
                    {
                        "name": "Kitchen Deep Cleaning",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "assets/img/cleaning.jpg",
                        "url": "assets/img/cleaning.jpg"
                    }
                ],
                "title": "Home Cleaning"
            },
            {
                "body": "Our Logistics experts will connect you with 3-4 trusted Packers and Movers of Delhi who will give you the best shifting quotes. Based on Budget, Time taken, Credibility of the Shifting Company, we advise our customers to make an informed decision. Past records and reviews of transporters also play an integral role.",
                "category": "shifting",
                "extraOptions": [
                    {
                        "name": "Carpenter",
                        "selected": false,
                        "value": "7"
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "assets/img/shifting1.jpeg",
                        "url": "assets/img/shifting1.jpeg"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Small Distance",
                        "value": "6"
                    },
                    {
                        "currency": "$",
                        "name": "Long Distance",
                        "value": "9"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Packers & Movers",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "assets/img/shifting.jpg",
                        "url": "assets/img/shifting.jpg"
                    }
                ],
                "title": "Shifting Homes"
            },
            {
                "body": "To most people, the idea of building a custom home is somewhat intimidating. We understand your concerns and have worked to relieve you of as much of the stress as possible when it comes to building your new home. As your partner in this endeavor, we will guide you through a process that breaks down the creation of your new home into manageable phases. This phased process should relieve most of your anxiety and help you accomplish your goals.",
                "category": "construction",
                "extraOptions": [
                    {
                        "name": "Furniture and Wood work",
                        "selected": false,
                        "value": "2.90"
                    },
                    {
                        "name": "Construction",
                        "selected": false,
                        "value": "7.50"
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "assets/img/construction1.jpeg",
                        "url": "assets/img/construction1.jpeg"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Small",
                        "value": "7"
                    },
                    {
                        "currency": "$",
                        "name": "Standard",
                        "value": "8.50"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Furniture and Wood work",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "assets/img/construction.jpeg",
                        "url": "assets/img/construction.jpeg"
                    }
                ],
                "title": "Home Design & Construction"
            },
            {
                "body": "As a full-service party decor & rental agency, it is our business to take the worries out of your party or event and make it stand out!",
                "category": "party",
                "extraOptions": [
                    {
                        "name": "House warming",
                        "selected": false,
                        "value": "3.80"
                    },
                    {
                        "name": "Bachelorette/bachelor party",
                        "selected": false,
                        "value": "4"
                    }
                ],
                "isFeatured": false,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "assets/img/party1.jpg",
                        "url": "assets/img/party1.jpg"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Half Day",
                        "value": "8.20"
                    },
                    {
                        "currency": "$",
                        "name": "Full Day",
                        "value": "13.50"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Birthday Party",
                        "selected": true
                    },
                    {
                        "name": "Wedding Reception",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "assets/img/party.jpg",
                        "url": "assets/img/party.jpg"
                    }
                ],
                "title": "Party and Event Services"
            }
        ]
		return this.menuList;
	};

    getEvents(){
        this.events = [
            {
                "deletable": false,
                "editable": false,
                "endsAt": 1505932200000,
                "incrementsBadgeTotal": false,
                "startsAt": 1505834129000,
                "title": "Hartford United States XL Center",
                "type": "info",
                "start": "2017-09-19T15:15:29.000Z",
                "end": "2017-09-20T18:30:00.000Z",
                "color": {
                    "primary": "#20ff60"
                }
            },
                {
                    "deletable": false,
                    "editable": false,
                    "endsAt": 1508943600000,
                    "incrementsBadgeTotal": false,
                    "startsAt": 1508925600000,
                    "title": "Trenton Sun National Bank Center",
                    "type": "important",
                    "start": "2017-10-25T10:00:00.000Z",
                    "end": "2017-10-25T15:00:00.000Z",
                    "color": {
                        "primary": "#ffd50a"
                    }
                },
                {
                    "deletable": false,
                    "editable": false,
                    "endsAt": 1434056400000,
                    "incrementsBadgeTotal": false,
                    "startsAt": 1433883600000,
                    "title": "Cincinnati US Bank Arena",
                    "type": "success",
                    "start": "2015-06-09T21:00:00.000Z",
                    "end": "2015-06-11T21:00:00.000Z",
                    "color": {
                        "primary": "#1e90ff"
                    }
                },
                {
                    "deletable": false,
                    "editable": false,
                    "endsAt": 1434402000000,
                    "incrementsBadgeTotal": false,
                    "startsAt": 1434229200000,
                    "title": "Milwaukee Marcus Amphitheater",
                    "type": "special",
                    "start": "2015-06-13T21:00:00.000Z",
                    "end": "2015-06-15T21:00:00.000Z",
                    "color": {
                        "primary": "#0008ff"
                    }
                }
            ];
        return this.events;

    };
}