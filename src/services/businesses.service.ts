import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import * as g from 'geolib';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class BusinessesService {
	businesses: any[];
	private currentBusiness: any;

	constructor(private geolocation: Geolocation) {
	}

	fetchBusinesses(filter: string, showFavorites: boolean, categoryId: string, orderBy: 'name' | 'distance' = 'name'): Promise<any[]> {
		filter = filter ? filter.toLowerCase() : filter;
		return this.getBusinesses().then(() => {
			let businesses = _.filter(this.businesses, (business: any) => {
				return (!filter || business.name.toLowerCase().indexOf(filter) >= 0)
					&& (!showFavorites || business.isInFavorites)
					&& (!categoryId || business.category === categoryId);
			});
			businesses = _.sortBy(businesses, business => business[orderBy]);

			return Promise.resolve(businesses);
		});
	}

	toggleFavorites(businessId): void {
		let toggle = !this.isInFavorites(businessId);

		_.each(this.businesses, (businessItem) => {
			if (businessItem.$key === businessId) {
				businessItem.isInFavorites = toggle;

				let favorites = this.getItem('favoritesBusinesses') || [];

				if (toggle) {
					favorites.push(businessItem.$key);
					favorites = _.uniq(favorites);
				} else {
					favorites = _.filter(favorites, (item) => item != businessItem.$key);
				}
				this.setItem('favoritesBusinesses', favorites);
			}
		});
	}

	getCategories(): Promise<any[]> {
        let category = [
            {
                "$key": null,
                "name": "All"
            },
            {
                "name": "Movie Theatres",
                "$key": "-KFmR066qUtvVv9IaBrq"
            },
            {
                "name": "Drink",
                "$key": "-KFmRyJu07Y6dVlVbdh9"
            },
            {
                "name": "Healthcare",
                "$key": "-KFoWA2ujTJ8Zb8JbaoZ"
            },
            {
                "name": "Food",
                "$key": "-KHKN0MiysoAodCMlYW_"
            },
            {
                "name": "Shop",
                "$key": "-KLYHKBftksPjoSHOEQ_"
            }
        ];
		return Promise.all([
			category])
        				.then(categories => Object.keys(categories).map(key => categories[key]));
	}

	isInFavorites(id): boolean {
		let favorites = this.getItem('favoritesBusinesses');
		return _.indexOf(favorites, id) >= 0;
	}

	getBusinesses(): Promise<any[]> {
		let business = [
            {
                "category": "-KFmRyJu07Y6dVlVbdh9",
                "description": "Celebrating the best of Bristol's brewing scene, we've got 8 hand pulls and 10 keg lines serving a huge range of craft beers from Bristol and around the UK. Only 5 minutes walk from Temple Meads Station and Old Market, we are the best pub around by a mile! A cosy pub, great beer garden and top notch pub food, you'll wish we were at the end of your road.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-07-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.7880446,
                            "longitude": -122.42192929999999,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.7891906,
                            "longitude": -122.42041870000003,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Barley Mow",
                "officeLocation": "37.7880446,-122.42192929999999",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-07.png"
                ],
                "rating": {
                    "reviews": 22,
                    "value": 4
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Drink",
                "distance": 12365158
            },
            {
                "category": "-KFoWA2ujTJ8Zb8JbaoZ",
                "description": "Cleveland Clinic – a global nonprofit academic medical centre has provided world class patient care to people from around the world since 1921. As the Canadian location of the Cleveland Clinic, we help our patients’ live healthier more active lives in a state-of-the-art outpatient clinic in downtown Toronto.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-01-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.794936,
                            "longitude": -122.424957,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.794732,
                            "longitude": -122.426544,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Cleveland Clinic",
                "officeLocation": "37.7736854,-122.421034",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-01.png"
                ],
                "rating": {
                    "reviews": 16,
                    "value": 4.3125
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "isInFavorites": true,
                "categoryName": "Healthcare",
                "distance": 12366695
            },
            {
                "category": "-KFoWA2ujTJ8Zb8JbaoZ",
                "description": "At Diagnostics Laboratories, our values are the core of who we are and what we believe in.  They inspire us to be our best every day and in everything we do in order to provide individuals, healthcare professionals and clients with the timely and accurate information they need to make informed decisions and improve patient outcomes.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-04-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.7889591,
                            "longitude": -122.4068924,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.7868542,
                            "longitude": -122.41235749999998,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Diagnostics Laboratories",
                "officeLocation": "37.7889591,-122.4068924",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-04.png"
                ],
                "rating": {
                    "reviews": 7,
                    "value": 4.428571428571429
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Healthcare",
                "distance": 12365480
            },
            {
                "category": "-KFmR066qUtvVv9IaBrq",
                "description": "When you join us, you can sit back and relax knowing you'll enjoy a one-of-a-kind movie experience. From crystal-clear, all digital projections to custom-built sound systems and stunning 3D, our theatres will immerse you in the movies!",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-05-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.78737020000001,
                            "longitude": -122.42342389999999,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.7894444,
                            "longitude": -122.42208419999997,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Edwards Theatres",
                "officeLocation": "37.78737020000001,-122.42342389999999",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-05.png"
                ],
                "rating": {
                    "reviews": 11,
                    "value": 4.545454545454546
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Movie Theatres",
                "distance": 12365187
            },
            {
                "category": "-KFoWA2ujTJ8Zb8JbaoZ",
                "description": "Genesis Fertility Centre is the first Canadian clinic to voluntarily undergo independent verification of success rates by IVF Reports and Fertility Authority, recognized leaders in IVF standards and patient advocacy.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-02-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.793139,
                            "longitude": -122.428991,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.791833,
                            "longitude": -122.432317,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Genesis Fertility Centre",
                "officeLocation": "37.9164026,-121.4234639",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-02.png"
                ],
                "rating": {
                    "reviews": 1,
                    "value": 4
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Healthcare",
                "distance": 12378677
            },
            {
                "category": "-KLYHKBftksPjoSHOEQ_",
                "description": "Home Craft's is more than a leading retailer of home decor and gifts. At Kirkland's, our unique products create the backdrop for memorable life events, like a family dinner, a child's first birthday, a 20th wedding anniversary, or just a cozy evening indoors.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-03-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.796229,
                            "longitude": -122.412221,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.793312,
                            "longitude": -122.412865,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Home Craft",
                "officeLocation": "37.8164026,-121.421164",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 0,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-03.png"
                ],
                "rating": {
                    "reviews": 12,
                    "value": 4.333333333333333
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Shop",
                "distance": 12389324
            },
            {
                "category": "-KFmR066qUtvVv9IaBrq",
                "description": "Two projectors run simultaneously to provide the perfect image with a balance of warmth and sharpness. IMAX draws you into something as close to reality as you have ever experienced. The combination of our perfectly tuned integrated sound system and our precise speaker orientation ensures you can hear a pin drop and be able to tell exactly where it landed.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-06-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.7875918,
                            "longitude": -122.42516369999998,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.7887371,
                            "longitude": -122.42371279999998,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "IMAX",
                "officeLocation": "37.7875918,-122.42516369999998",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-06-01.png",
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-06-02.png"
                ],
                "rating": {
                    "reviews": 6,
                    "value": 4.666666666666667
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Movie Theatres",
                "distance": 12365116
            },
            {
                "category": "-KHKN0MiysoAodCMlYW_",
                "description": "In japanese, our name means ‘naughty child’, or ‘one who is wishful and determined’. A good example of that determination is the art of ‘kaizen’, meaning ‘good change’. This philosophy sits right at our heart. It shapes every dish we create, and pushes us to find better ways in all that we do. We’re restless spirits, forever creating and making things better. We’ve been practising kaizen since 1992, when we opened our first doors in london’s bloomsbury.",
                "drupal": "https://demo.titaniumtemplates.com/drupal/rest/views/rest_api",
                "email": "sofia.atsalou@gmail.com",
                "facebookPage": "https://www.facebook.com/ionicframework",
                "logo": "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-08-logo.png",
                "mapdata": {
                    "annotations": [
                        {
                            "latitude": 37.7975623,
                            "longitude": -122.4054395,
                            "title": "Molestie et wisi."
                        },
                        {
                            "latitude": 37.7971271,
                            "longitude": -122.4089501999999,
                            "title": "Ullamcorper eros."
                        }
                    ]
                },
                "name": "Wagamama",
                "officeLocation": "37.7975623,-122.4054395",
                "openhours": {
                    "days": [
                        {
                            "closeAt": 1420124400000,
                            "day": 1,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420110000000,
                            "day": 2,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420138800000,
                            "day": 2,
                            "openAt": 1420124400000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 3,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420126300000,
                            "day": 4,
                            "openAt": 1420095600000
                        },
                        {
                            "closeAt": 1420124400000,
                            "day": 5,
                            "openAt": 1420095600000
                        }
                    ],
                    "zone": 3
                },
                "phoneNumber": "+398764123456",
                "pictures": [
                    "https://skounis.s3.amazonaws.com/mobile-apps/business-directory/assets/business-08.png"
                ],
                "rating": {
                    "reviews": 8,
                    "value": 4.125
                },
                "wordpress": "https://demo.titaniumtemplates.com/wordpress/?json=1",
                "categoryName": "Food",
                "distance": 12364614
            }
        ];
		let category = [
            {
                "$key": null,
                "name": "All"
            },
            {
                "name": "Movie Theatres",
                "$key": "-KFmR066qUtvVv9IaBrq"
            },
            {
                "name": "Drink",
                "$key": "-KFmRyJu07Y6dVlVbdh9"
            },
            {
                "name": "Healthcare",
                "$key": "-KFoWA2ujTJ8Zb8JbaoZ"
            },
            {
                "name": "Food",
                "$key": "-KHKN0MiysoAodCMlYW_"
            },
            {
                "name": "Shop",
                "$key": "-KLYHKBftksPjoSHOEQ_"
            }
        ];
		if (!this.businesses) {
			return Promise.all([business, category])
				.then(([businesses, categories]) => {
					this.businesses = businesses;

					let favorites = this.getItem('favoritesBusinesses') || [];
					_.each(businesses, (business) => {
						if (favorites.indexOf(business.$key) >= 0) {
							business.isInFavorites = true;
						}
						console.log(categories)
						business.categoryName = category.find(function (obj) { return obj.$key === business.category; }).name;

					});

					this.setDistance(businesses);

					return businesses;
				});
		}
		return Promise.resolve(this.businesses);
	}

	private setDistance(businesses: any) {
		let origins = _.map(businesses, (business: any) => {
			return business.officeLocation;
		});
		businesses = _.map(businesses, (business) => business);

		this.getDistancesToOrigins(origins)
			.then((distances) => {
				_.each(businesses, (business, index) => {
					business.distance = distances[index];
				});
			});
	}

	setCurrent(business: any) {
		this.currentBusiness = business;
	}

	getCurrent() {
		return this.currentBusiness;
	}

    getDistancesToOrigins(origins) {
        let posOptions = {
            enableHighAccuracy: true
        };
        return this.geolocation.getCurrentPosition(posOptions)
            .then((position) => {
                return _.map(origins, (origin) => {
                    if (!origin) {
                        return null;
                    }
                    return this.getDistance(origin, position);
                });
            });
    }

    getDistanceToOrigin(origin) {
        let posOptions = {
            enableHighAccuracy: true
        };
        return this.geolocation.getCurrentPosition(posOptions)
            .then((position) => this.getDistance(origin, position));
    }

    getDistance(origin, position) {
        origin = origin.split(',');
        origin = {
            latitude: origin[0],
            longitude: origin[1]
        };

        let distance = g.getDistance({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        }, origin);

        /*
        if (distance < 1000) {
            distance = distance + ' m';
        } else {
            distance = convert(distance, 'meters', {
                precision: 2
            }).toKilometers() + ' km';
        }
        */
        return distance;
    }

    // getCurrentPosition() {
    //     let posOptions = {
    //         enableHighAccuracy: true
    //     };
    //
    //     return this.geolocation.getCurrentPosition(posOptions);
    // }

     getItem(key): any {
        let json = localStorage.getItem(key);
        return json ? JSON.parse(json) : null;
    }

     setItem(key, value): void {
        localStorage.setItem(key, JSON.stringify(value));
    }
}